﻿using CodeMarking.Presentation.Order;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CodeMarking.UI.Order
{
    /// <summary>
    /// Логика взаимодействия для OrderListPage.xaml
    /// </summary>
    public partial class OrderListPage : Page
    {
        public OrderListPage()
        {
            InitializeComponent();
            var vm = MainApp.Current.Container.Get<OrderListViewModel>();
            checkStatus.Click += (sender, args) => vm.OnCheckStatusClicked();
            cancelOrder.Click += (sender, args) => vm.OnCloseOrderClicked();
            getBarcodes.Click += (sender, args) => vm.GetBarcodesClicked();
            printCodes.Click += (sender, args) => vm.PrintCodes();
            DataContext = vm;
        }
    }
}
