﻿using CodeMarking.Presentation.Order;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CodeMarking.UI.Order
{
    /// <summary>
    /// Логика взаимодействия для CreateOrderDialog.xaml
    /// </summary>
    public partial class CreateOrderDialog : Window
    {
        public CreateOrderDialog()
        {
            InitializeComponent();
            var vm = MainApp.Current.Container.Get<OrderCreateViewModel>();
            vm.OnRequestClose += (s, e) =>
            {
                this.DialogResult = true;
                this.Close();
            };
            DataContext = vm;
        }
    }
}
