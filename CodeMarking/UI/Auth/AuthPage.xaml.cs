﻿using CodeMarking.Presentation.Auth;
using CodeMarking.Tools.Printer;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static CodeMarking.Tools.Printer.GoodexPrinter;

namespace CodeMarking.UI.Auth
{
    /// <summary>
    /// Логика взаимодействия для AuthPage.xaml
    /// </summary>
    public partial class AuthPage : Page
    {
        public AuthPage()
        {
            InitializeComponent();
            DataContext = MainApp.Current.Container.Get<AuthViewModel>();
            Init();
        }

        private void Init()
        {
            OneBtn.Click += (arg, sender) => AppendPassword('1');
            TwoBtn.Click += (arg, sender) => AppendPassword('2');
            ThreeBtn.Click += (arg, sender) => AppendPassword('3');
            FourBtn.Click += (arg, sender) => AppendPassword('4');
            FiveBtn.Click += (arg, sender) => AppendPassword('5');
            SixBtn.Click += (arg, sender) => AppendPassword('6');
            SevenBtn.Click += (arg, sender) => AppendPassword('7');
            EightBtn.Click += (arg, sender) => AppendPassword('8');
            NineBtn.Click += (arg, sender) => AppendPassword('9');
            ZeroBtn.Click += (arg, sender) => AppendPassword('0');
            BackspaceBtn.Click += (arg, sender) => RemoveLastChar();

          
        }

        private void AppendPassword(char s)
        {
            var pass = txtPassword.Password;
            pass = string.Concat(pass, s);
            txtPassword.Password = pass;
        }

        private void RemoveLastChar()
        {
            var pass = txtPassword.Password;
            if (!string.IsNullOrEmpty(pass))
                txtPassword.Password = pass.Remove(pass.Length - 1);
        }
    }
}
