﻿using CodeMarking.Presentation.Main;
using CodeMarking.Tools.Navigator;
using Ninject;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer.Discovery;

namespace CodeMarking.UI.Main
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var navigator = MainApp.Current.Container.Get<INavigator>();
            navigator.InitNavigationService(FrameLayout.NavigationService);
            DataContext = MainApp.Current.Container.Get<GlobalViewModel>();
            var printers = UsbDiscoverer.GetZebraUsbPrinters();
            var zPrinters = UsbDiscoverer.GetZebraUsbPrinters(new ZebraPrinterFilter());
        }
    }
}
