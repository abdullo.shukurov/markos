﻿using CodeMarking.Presentation.Scanner;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CodeMarking.UI.Scanner
{
    /// <summary>
    /// Логика взаимодействия для ScannerPage.xaml
    /// </summary>
    public partial class ScannerPage : Page
    {
        ScannerViewModel vm;
        public ScannerPage()
        {
            InitializeComponent();
             vm = MainApp.Current.Container.Get<ScannerViewModel>();
            DataContext = vm;
        }

        private void BarcodeTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                vm.TextChange(BarcodeTxt.Text);
            }
        }

        private void BackspaceBtn_Click(object sender, RoutedEventArgs e)
        {
            BarcodeTxt.Text = "";
        }
    }
}
