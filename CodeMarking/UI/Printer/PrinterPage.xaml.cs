﻿using CodeMarking.Presentation.Printer;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CodeMarking.UI.Printer
{
    /// <summary>
    /// Логика взаимодействия для PrinterPage.xaml
    /// </summary>
    public partial class PrinterPage : Page
    {
        private bool isBarcodeEditing = true;
        public PrinterPage()
        {
            InitializeComponent();
            Init();
            var vm = MainApp.Current.Container.Get<PrinterViewModel>();
            DataContext = vm;
        }


        private void BarcodeTxt_GotFocus(object sender, RoutedEventArgs e)
        {
            isBarcodeEditing = true;
        }

        private void CountexTxt_GotFocus(object sender, RoutedEventArgs e)
        {
            isBarcodeEditing = false;
        }

        private void Init()
        {
            OneBtn.Click += (arg, sender) => AppendChar('1');
            TwoBtn.Click += (arg, sender) => AppendChar('2');
            ThreeBtn.Click += (arg, sender) => AppendChar('3');
            FourBtn.Click += (arg, sender) => AppendChar('4');
            FiveBtn.Click += (arg, sender) => AppendChar('5');
            SixBtn.Click += (arg, sender) => AppendChar('6');
            SevenBtn.Click += (arg, sender) => AppendChar('7');
            EgithBtn.Click += (arg, sender) => AppendChar('8');
            NineBtn.Click += (arg, sender) => AppendChar('9');
            ZeroBtn.Click += (arg, sender) => AppendChar('0');
            BackspaceBtn.Click += (arg, sender) => RemoveLastChar();


        }

        private void AppendChar(char s)
        {
            var text = isBarcodeEditing ? BarcodeTxt.Text : CountexTxt.Text;
            text = string.Concat(text, s);
            if (isBarcodeEditing) BarcodeTxt.Text = text; else CountexTxt.Text = text;
        }

        private void RemoveLastChar()
        {
            var text = isBarcodeEditing ? BarcodeTxt.Text : CountexTxt.Text;
            if (string.IsNullOrEmpty(text)) return;
            text = text.Remove(text.Length - 1);
            if (isBarcodeEditing) BarcodeTxt.Text = text; else CountexTxt.Text = text;
        }
    }
}
