﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CodeMarking.UI.Tools
{
    /// <summary>
    /// Логика взаимодействия для CounterDialog.xaml
    /// </summary>
    public partial class CounterDialog : Window
    {
        public int Count { get; set; } = 0;
        public CounterDialog(string title)
        {
            InitializeComponent();
            this.Title = title;
            ApplyButton.Click += ApplyClicked;
        }

        private void ApplyClicked(object sender, RoutedEventArgs e)
        {
            if (int.TryParse(CountexTxt.Text, out int count))
            {
                Count = count;
                this.DialogResult = true;
                this.Close();
            }
        }
    }
}
