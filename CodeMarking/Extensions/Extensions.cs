﻿using FluentResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodeMarking.Extensions
{
  public static  class Extensions
    {
        public static string ToUserMessage(this List<Error> errors)
        {
            if (errors.Count == 0)
                return "Произошла ошибка ";
            return string.Join("\n", errors.Select(x => x.Message));
        }

        public static List<string> Match(this string data, string pattern, RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Multiline)
        {
            Regex r = new Regex(pattern, options);
            MatchCollection matches = r.Matches(data);
            if (matches.Count > 0)
                return matches.Cast<Match>().Select(x => x.Value).ToList();
            else return new List<string>();
        }
    }
}
