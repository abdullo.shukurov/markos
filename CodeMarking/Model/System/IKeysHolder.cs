﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.System
{
    public interface IKeysHolder
    {
        string GatewayUrl { get; }
        string omsId { get; set; }
        string clientToken { get; set; }
        int XPrintCoordinate { get; set; }
        int YPrintCoordinate { get; set; }
        string PrinterType { get; set; }
        string ProductExtension { get; set; }
        DateTime? SubscritionStartDate { get; set; }
        DateTime? SubscriptionEndDate { get; set; }
        bool Save();
    }
}
