﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.System
{
    public interface IResourceManager
    {
        string GetStringRes(string name);
    }
}
