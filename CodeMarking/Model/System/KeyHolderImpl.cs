﻿using GKeys;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.System
{
    public class KeyHolderImpl : IKeysHolder
    {
        private GYek keys;

        [Inject]
        public KeyHolderImpl(GYek keys)
        {
            this.keys = keys;
        }

        public string GatewayUrl { get => "https://omscloud.asllikbelgisi.uz"; }
        public string omsId { get => keys.omsId; set => keys.omsId = value; }
        public string PrinterType { get => keys.printerType; set => keys.printerType = value; }
        public string ProductExtension { get => keys.productExtension; set => keys.productExtension = value; }

        public string clientToken { get => keys.token; set => keys.token = value; }
        public int XPrintCoordinate { get => keys.XPrintCoordinate; set => keys.XPrintCoordinate = value; }
        public int YPrintCoordinate { get => keys.YPrintCoordinate; set => keys.YPrintCoordinate = value; }
        public DateTime? SubscriptionEndDate
        {
            get
            {
                DateTime date;
                DateTime.TryParse(keys.SubscriptionEndDate, out date);
                return date;
            }
            set
            {
                keys.SubscriptionEndDate = value?.ToString("yyyy-MM-dd'T'HH:mm:ss");
            }
        }
        public DateTime? SubscritionStartDate
        {
            get
            {
                DateTime date;
                DateTime.TryParse(keys.SubscriptionStartDate, out date);
                return date;
            }
            set
            {
                keys.SubscriptionStartDate = value?.ToString("yyyy-MM-dd'T'HH:mm:ss");
            }
        }

        public bool Save() => keys.SaveChanges();
    }
}
