﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CodeMarking.Model.System
{
    public class ResourceManager : IResourceManager
    {
        public string GetStringRes(string name)
        {
            return Application.Current.Resources[name] as string;
        }
    }
}
