﻿using CodeMarking.Entities;
using CodeMarking.UI.Order;
using CodeMarking.UI.Printer;
using CodeMarking.UI.Scanner;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace CodeMarking.Model.Interactor.Main
{
    public class MenuInteractor
    {
        public delegate void ChangePageDelegate(string title, object data);
        public event ChangePageDelegate OnChangePage;
        public const string PAGE_ORDER = "Заказы";
        public const string PAGE_AGGREGATION = "Агрегация";
        public const string PAGE_SCANNER = "Сканнер";
        public const string PAGE_PRINTER = "Принтер";

        [Inject]
        public MenuInteractor() { }

        public List<MenuItem> MenuItems() => new List<MenuItem>()
        {
            new MenuItem { Title = PAGE_ORDER, Page = new OrderListPage()}
        };

        public void ChangePage(string page, object data)
        {
            OnChangePage?.Invoke(page, data);
        }
    }
}
