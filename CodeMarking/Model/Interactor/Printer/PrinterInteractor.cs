﻿using CodeMarking.Model.Repository.Code;
using CodeMarking.Model.Repository.Order;
using CodeMarking.Tools.Printer;
using FluentResults;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Interactor.Printer
{
    public class PrinterInteractor
    {
        private CodeRepository codeRepository;
        private OrderRepository orderRepository;
        private PrinterFactory printFactory;

        [Inject]
        public PrinterInteractor(CodeRepository codeRepository, OrderRepository orderRepository, PrinterFactory printer)
        {
            this.codeRepository = codeRepository;
            this.orderRepository = orderRepository;
            this.printFactory = printer;
        }

        public Task<Result<List<Entities.Codes>>> GetNotPrintedCodes(Entities.Order order, int count, int lastId) => codeRepository.GetNotPrintedCodes(order, count, lastId);

        public async  Task<Result> PrintCodes(string code, int count,string cisType) {

            var codesResult = await codeRepository.GetNotPrintedCodes(code, count, cisType);
            if (codesResult.IsFailed) return codesResult;

            var printerResult = printFactory.Printer.PrintCodes(codesResult.ValueOrDefault);
            if (printerResult.IsFailed) return printerResult;
            var updateStatusResult = await codeRepository.UpdateCodesPrintStatus(codesResult.ValueOrDefault, printerResult.IsSuccess);
            return updateStatusResult;
         
        }

        public async Task<Result> PrintCodes(Entities.Order order, int count)
        {
            var codesResult = await codeRepository.GetNotPrintedCodes(order, count, 0);
            if (codesResult.IsFailed) return codesResult;

            var printerResult = printFactory.Printer.PrintCodes(codesResult.ValueOrDefault);
            if (printerResult.IsFailed) return printerResult;
            var updateStatusResult = await codeRepository.UpdateCodesPrintStatus(codesResult.ValueOrDefault, printerResult.IsSuccess);
            if (updateStatusResult.IsFailed) return updateStatusResult;
            var orderPrintedResultCount = await orderRepository.UpdatePrintedCount(order.orderId, count);
            return orderPrintedResultCount;
        }

    }
}
