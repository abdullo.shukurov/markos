﻿using CodeMarking.Entities;
using CodeMarking.Model.Repository.Code;
using FluentResults;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Interactor.Scanner
{
    public class ScannerInteractor
    {
        private CodeRepository codeRepository;

        [Inject]
        public ScannerInteractor(CodeRepository codeRepository)
        {
            this.codeRepository = codeRepository;
        }


        public Task<Result> AddScannedCode(string code) => codeRepository.AddScanedCode(code);

        public Task<Result<List<string>>> AddScannedCodes(List<string> codes) => codeRepository.AddScanedCodes(codes);
    }
}
