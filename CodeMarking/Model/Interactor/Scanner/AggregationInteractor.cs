﻿using CodeMarking.Entities;
using CodeMarking.Model.Repository.Aggregation;
using FluentResults;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Interactor.Scanner
{
   public  class AggregationInteractor
    {
        private AggregationRepository aggregationRepository;

        [Inject]
        public AggregationInteractor(AggregationRepository aggregationRepository) {
            this.aggregationRepository = aggregationRepository;
        }

        public Task<Result> AddAggregation(Aggregation aggregation) => aggregationRepository.AddAggregation(aggregation);
    }
}
