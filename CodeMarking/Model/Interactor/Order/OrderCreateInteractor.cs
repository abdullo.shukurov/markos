﻿using CodeMarking.Entities;
using CodeMarking.Model.Repository.Order;
using FluentResults;
using Marker.Model.Repository.Billing;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Interactor.Order
{
    public class OrderCreateInteractor
    {
        private OrderRepository orderRepository;
        private BillingRepository billingRepository;

        [Inject]
        public OrderCreateInteractor(OrderRepository orderRepository, BillingRepository billingRepository)
        {
            this.orderRepository = orderRepository;
            this.billingRepository = billingRepository;
        }

        public int TemplateId(string cisType) => orderRepository.TemplateId(cisType);

        public async Task<Result<Entities.Order>> CreateOrder(OrderRequest orderRequest)
        {
            if (!CheckSubscription())
                return Result.Fail("У вас просрочено подписка. Свяжитесь с разработчиками программы ");
            return await orderRepository.CreateOrder(orderRequest);
        }

        private bool CheckSubscription()
        {
            var endDate = billingRepository.SubscriptionEndDate;
            var now = DateTime.Now;
            if (endDate == null) return true;
            return now > endDate;
        }
    }
}
