﻿using CodeMarking.Model.Repository.Code;
using CodeMarking.Model.Repository.Order;
using FluentResults;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Interactor.Order
{
    public class OrderListInteractor
    {
        private OrderRepository orderRepository;
        private CodeRepository codeRepository;

        [Inject]
        public OrderListInteractor(OrderRepository orderRepository, CodeRepository codeRepository)
        {
            this.orderRepository = orderRepository;
            this.codeRepository = codeRepository;
        }

        public Task<Result<List<Entities.Order>>> AllOders() => orderRepository.AllOrders();

        public Task<Result<Entities.OrderStatus>> CheckStatus(Entities.OrderStatusRequest request) => orderRepository.CheckStatus(request);

        public Task<Result<Entities.OrderCloseResult>> CloseOrder(Entities.Order order) => orderRepository.CloseOrder(order);

        public Task<Result<Entities.CodeResult>> GetOrderCodes(Entities.CodeRequest request) => codeRepository.GetCodes(request);

    }
}
