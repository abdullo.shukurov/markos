﻿using CodeMarking.Model.Repository.Aggregation;
using CodeMarking.Model.Repository.Code;
using CodeMarking.Tools.Log;
using FluentResults;
using Marker.Model.Repository.Billing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Interactor.Sync
{
    public class SyncInteractor
    {
        private CodeRepository codeRepository;
        private BillingRepository billingRepository;
        private AggregationRepository aggregationRepository;
        private Logger logger;
        public SyncInteractor(CodeRepository codeRepository, AggregationRepository aggregationRepository, BillingRepository billingRepository, Logger logger)
        {
            this.codeRepository = codeRepository;
            this.aggregationRepository = aggregationRepository;
            this.billingRepository = billingRepository;
            this.logger = logger;
        }

        public async Task<Result> SyncScannedCodes()
        {
            var codeResult = await codeRepository.SyncScannedCodes(30);
            logger.WriteLog("Синхронизация сканированных кодов", codeResult);

            var aggregationResult = await aggregationRepository.SyncAggregations(5);
            logger.WriteLog("Синхронизация кодов агрегации", aggregationResult);
            return codeResult;
        }

        public async Task<Result> SyncBilling()
        {
            var billingResult = await billingRepository.SyncSubscription();
            logger.WriteLog("Синхронизация", billingResult);
            return billingResult;
        }
    }
}
