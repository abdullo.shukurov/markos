﻿using CodeMarking.Entities;
using CodeMarking.Model.Repository;
using FluentResults;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Interactor.Auth
{
    public class AuthInteractor
    {
        private AuthRepository authRepository;

        [Inject]
        public AuthInteractor(AuthRepository authRepository) {
            this.authRepository = authRepository;
        }

        public Task<Result<User>> Auth(string password) => authRepository.Auth(password);
    }
}
