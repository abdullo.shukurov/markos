﻿using CodeMarking.Entities;
using FluentResults;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Repository
{
    public class AuthRepository
    {

        public async Task<Result<User>> Auth(string password)
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    var users = await entity.User.ToListAsync();
                    if (users.Count == 0)
                    {
                        users.Add(new User() { name = "Admin", password = "0000", role = "admin" });
                        users.Add(new User() { name = "Scanner", password = "0001", role = "scanner" });
                        users.Add(new User() { name = "Printer", password = "0002", role = "printer" });
                        entity.User.AddRange(users);
                        await entity.SaveChangesAsync();
                    }
                    var user = await entity.User.FirstOrDefaultAsync(x => x.password == password);
                    if (user != null) return user.ToResult();
                    return Result.Fail("Incorrect password");
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }

        }
    }
}
