﻿using CodeMarking.Entities;
using CodeMarking.Model.Server;
using FluentResults;
using Ninject;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Repository.Aggregation
{
    public class AggregationRepository
    {
        private IApi api;
        [Inject]
        public AggregationRepository(IApi api)
        {
            this.api = api;
        }

        public async Task<Result> AddAggregation(Entities.Aggregation aggregation)
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    aggregation.sntinJson = string.Join(";;;", aggregation.sntins.Select(x => x.Substring(0, 25)));
                    entity.Aggregation.AddOrUpdate(aggregation);
                    await entity.SaveChangesAsync();
                    return Result.Ok();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Result.Fail(ex.Message);
            }
        }

        public async Task<Result<int>> SyncAggregations(int count)
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    var aggregations = await entity.Aggregation
                        .Where(x => x.isSynced != 1)
                        .Take(count)
                        .ToListAsync();
                    if (aggregations?.Count == 0) return 0.ToResult();

                    aggregations.ForEach(x => x.sntins = x.sntinJson.Split(new string[] { ";;;" }, StringSplitOptions.None).ToList());

                    var request = new AggregationRequest()
                    {
                        aggregationUnits = aggregations,
                        participantId = "307966715"
                    };

                    var result = await api.SendAggregationCodes(request);
                    if (result.IsSuccessful)
                    {
                        aggregations.ForEach(x => x.isSynced = 1);
                        await entity.SaveChangesAsync();
                    }
                    return request.aggregationUnits.Count.ToResult();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Result.Fail(ex.Message);
            }
        }


    }
}
