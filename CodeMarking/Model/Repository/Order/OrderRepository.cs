﻿using CodeMarking.Entities;
using CodeMarking.Model.Server;
using CodeMarking.Model.System;
using FluentResults;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Repository.Order
{
    public class OrderRepository
    {
        private IApi api;
        private IKeysHolder keys;

        public OrderRepository(IApi api, IKeysHolder keys)
        {
            this.api = api;
            this.keys = keys;
        }

        public async Task<Result<Entities.Order>> CreateOrder(OrderRequest request)
        {
            try
            {
                var result = await api.CreateOrder(request);
                if (result.IsSuccessful)
                {
                    var product = request.products.FirstOrDefault();
                    result.Data.gtin = product?.gtin;
                    result.Data.name = product?.name;
                    result.Data.cisType = product.cisType;
                    result.Data.status = ORDER_STATUS.CREATED;
                    result.Data.maxCount = product?.quantity ?? 0;
                    await SaveOrder(result.Data);
                    return result.Data.ToResult();
                }
                return Result.Fail(result.Content);
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }

        public int TemplateId(string cisType)
        {
            if (keys.ProductExtension == PRODUCT_EXTENSION.BEER)
            {
                if (cisType == CIS_TYPE.UNIT) return 18;
                else return 19;
            }
            else if (keys.ProductExtension == PRODUCT_EXTENSION.ALCOHOL)
            {
                if (cisType == CIS_TYPE.UNIT)
                    return 13;
                else return 17;
            }
            return 0;
        }

        public async Task<Result<OrderStatus>> CheckStatus(OrderStatusRequest request)
        {
            try
            {
                var result = await api.CheckOrderStatus(request);
                if (result.IsSuccessful)
                {
                    await UpdateStatus(result.Data.orderId, result.Data.bufferStatus);
                    return result.Data.ToResult();
                }
                return Result.Fail(result.Content);
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }


        public async Task<Result<OrderCloseResult>> CloseOrder(Entities.Order order)
        {
            try
            {
                var result = await api.CloseOrder(order);
                if (result.IsSuccessful)
                {
                    await UpdateStatus(order.orderId, ORDER_STATUS.CLOSED);
                    return result.Data.ToResult();
                }
                return Result.Fail(result.Content);
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }


        public async Task SaveOrder(Entities.Order order)
        {
            using (var entity = new EntityContext())
            {
                _ = entity.Orders.Add(order);
                await entity.SaveChangesAsync();
            }
        }

        public async Task UpdateStatus(string orderId, string status)
        {
            using (var entity = new EntityContext())
            {
                var saved = entity.Orders.FirstOrDefault(x => x.orderId == orderId);
                saved.status = status;
                await entity.SaveChangesAsync();
            }
        }

        public async Task<Result> UpdatePrintedCount(string orderId, int count)
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    var saved = entity.Orders.FirstOrDefault(x => x.orderId == orderId);
                    saved.printed += count;
                    await entity.SaveChangesAsync();
                    return Result.Ok();
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }
        }


        public async Task<Result<List<Entities.Order>>> AllOrders()
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    return (await entity.Orders.ToListAsync()).ToResult();
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }
        }
    }
}
