﻿using CodeMarking.Entities;
using CodeMarking.Extensions;
using CodeMarking.Model.Server;
using FluentResults;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CodeMarking.Model.Repository.Code
{
    public class CodeRepository
    {
        private IApi api;
        public CodeRepository(IApi api)
        {
            this.api = api;
        }

        public async Task<Result<CodeResult>> GetCodes(CodeRequest request)
        {
            try
            {
                var result = await api.GetOrderCodes(request);
                if (result.IsSuccessful)
                {
                    await SaveCodes(result.Data, request);
                    await UpdateOrderBlockId(result.Data, request);
                    return result.Data.ToResult();
                }
                return Result.Fail(result.Content);
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }

        public async Task<Result<List<Codes>>> GetNotPrintedCodes(Entities.Order order, int count, int lastId)
        {
            try
            {
                using (var entity = new EntityContext())
                {

                    var result = await entity.Codes
                        .OrderBy(x => x.id)
                        .Where(x => x.orderId == order.orderId && x.id > lastId)
                        .Take(count)
                        .ToListAsync();
                    return result.ToResult();
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }

        public async Task<Result<List<Codes>>> GetNotPrintedCodes(string gtin, int count, string cisType)
        {
            try
            {
                using (var entity = new EntityContext())
                {

                    var result = await entity.Codes
                        .OrderBy(x => x.id)
                        .Where(x => x.gtin == gtin && x.isPrinted == 0 && x.cisType == cisType)
                        .Take(count)
                        .ToListAsync();
                    return result.ToResult();
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }

        public async Task<Result> UpdateCodesPrintStatus(List<Codes> codes, bool isPrint)
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    var min = codes.Min(x => x.id);
                    var max = codes.Max(x => x.id);
                    var orderId = codes.First().orderId;
                    var saved = await entity.Codes.Where(x => x.id >= min && x.id <= max).ToListAsync();
                    saved.ForEach(x => x.isPrinted = isPrint ? 1 : 0);
                    await entity.SaveChangesAsync();
                    return Result.Ok();
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }

        public async Task<Result> AddScanedCode(string code)
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    entity.ScannedCodes.Add(new ScannedCodes() { code = code, dateStr = DateTime.Now.ToString("dd-MM-yyyy") });
                    await entity.SaveChangesAsync();
                    return Result.Ok();
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }
        public async Task<Result<List<string>>> AddScanedCodes(List<string> codes)
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    codes.ForEach(code => entity.ScannedCodes.AddOrUpdate(new ScannedCodes() { code = code, dateStr = DateTime.Now.ToString("dd-MM-yyyy") }));
                    await entity.SaveChangesAsync();
                    return Result.Ok();
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }

        public async Task<Result<List<ScannedCodes>>> ScannedCodes(string date)
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    return (await entity.ScannedCodes
                         .Where(x => x.dateStr == date)
                         .ToListAsync())
                         .ToResult();
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }


        public async Task<Result<int>> SyncScannedCodes(int count)
        {
            try
            {
                using (var entity = new EntityContext())
                {
                    var notSynced = await entity.ScannedCodes
                        .Where(x => x.isSynced != 1)
                        .OrderBy(x => x.id)
                        .Take(count).ToListAsync();
                    if (notSynced.Count == 0) return Result.Ok(count);
                    var request = new Utilisation()
                    {
                        sntins = notSynced.Select(x => x.code).ToList(),
                        usageType = "VERIFIED",
                        brandcode = "2212Brandcode",
                        productionLineId = "1",
                        productionOrderId = "123"
                    };
                    var result = await api.UtilisatonCode(request);
                    if (result.IsSuccessful)
                    {
                        notSynced.ForEach(x => x.isSynced = 1);
                        await entity.SaveChangesAsync();
                        return Result.Ok(notSynced.Count);
                    }
                    return Result.Fail(result.Content);
                }
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }

        public async Task SaveCodes(CodeResult codes, CodeRequest request)
        {
            using (var entity = new EntityContext())
            {
                var markingCodes = codes.codes.Select(x => new Codes()
                {
                    code = x,
                    orderId = request.orderId,
                    cisType = request.cisType,
                    blockId = codes.blockId,
                    gtin = request.gtin,
                });
                entity.Codes.AddRange(markingCodes);
                await entity.SaveChangesAsync();
            }
        }

        public async Task UpdateOrderBlockId(CodeResult codes, CodeRequest request)
        {
            using (var entity = new EntityContext())
            {
                var order = await entity.Orders.FirstOrDefaultAsync(x => x.orderId == request.orderId);
                order.saved += codes.codes.Count;
                order.lastBlockId = codes.blockId;
                await entity.SaveChangesAsync();
            }
        }
    }
}
