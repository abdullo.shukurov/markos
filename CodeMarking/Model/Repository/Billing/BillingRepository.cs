﻿using CodeMarking.Model.Server;
using CodeMarking.Model.System;
using FluentResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marker.Model.Repository.Billing
{
    public class BillingRepository
    {
        private IApi api;
        private IKeysHolder keysHolder;

        public BillingRepository(IApi api, IKeysHolder keysHolder)
        {
            this.api = api;
            this.keysHolder = keysHolder;
        }

        public async Task<Result> SyncSubscription()
        {
            try
            {
                var result = await api.Subscription(keysHolder.omsId);
                if (result.IsSuccessful)
                {
                    DateTime.TryParse(result.Data.start_date, out DateTime startDate);
                    DateTime.TryParse(result.Data.end_date, out DateTime endDate);
                    keysHolder.SubscritionStartDate = startDate;
                    keysHolder.SubscriptionEndDate = endDate;
                    return Result.Ok();
                }
                return Result.Fail(result.Content);
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.ToString());
            }
        }

        public DateTime? SubscriptionStartDate => keysHolder.SubscritionStartDate;
        public DateTime? SubscriptionEndDate => keysHolder.SubscriptionEndDate;
    }
}
