﻿using CodeMarking.Entities;
using CodeMarking.Model.System;
using Marker.Entities;
using Newtonsoft.Json;
using Ninject;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CodeMarking.Model.Server
{
    public class ApiImpl : IApi
    {

        private IKeysHolder keys;

        private string BASE_URL => "https://omscloud.aslbelgisi.uz/api/v2/" + keys.ProductExtension;
        private string BILLING_ENDPOINT=> "http://api.fbox.uz/";


        [Inject]
        public ApiImpl(IKeysHolder keys)
        {
            this.keys = keys;
        }

        public Task<IRestResponse<OrderStatus>> CheckOrderStatus(OrderStatusRequest request)
        {
            var restRequest = new RestRequest($"/buffer/status?orderId={request.order.orderId}&omsId={keys.omsId}&gtin={request.gtin}", Method.GET);
            var cancellationTokenSource = new CancellationTokenSource();
            return ApiClient().ExecuteAsync<OrderStatus>(restRequest, cancellationTokenSource.Token);
        }

        public Task<IRestResponse<Order>> CreateOrder(OrderRequest request)
        {
            var restRequest = new RestRequest($"/orders?omsId={keys.omsId}", Method.POST);
            var json = JsonConvert.SerializeObject(request);
            restRequest.AddJsonBody(json);
            var cancellationTokenSource = new CancellationTokenSource();
            return ApiClient().ExecuteAsync<Order>(restRequest, cancellationTokenSource.Token);
        }

        public Task<IRestResponse<OrderCloseResult>> CloseOrder(Order order)
        {
            var restRequest = new RestRequest($"/buffer/close?orderId={order.orderId}&gtin={order.gtin}&omsId={order.omsId}&lastBlockId={order.lastBlockId}", Method.POST);
            var cancellationTokenSource = new CancellationTokenSource();
            return ApiClient().ExecuteAsync<OrderCloseResult>(restRequest, cancellationTokenSource.Token);
        }

        public Task<IRestResponse<CodeResult>> GetOrderCodes(CodeRequest request)
        {
            var restRequest = new RestRequest($"/codes?omsId={request.omsId}&orderId={request.orderId}&gtin={request.gtin}&quantity={request.quantity}&lastBlockId={request.lastBlockId}", Method.GET);
            var cancellationTokenSource = new CancellationTokenSource();
            return ApiClient().ExecuteAsync<CodeResult>(restRequest, cancellationTokenSource.Token);
        }

        public Task<IRestResponse<CodeResult>> UtilisatonCode(Utilisation request)
        {
            var restRequest = new RestRequest($"/utilisation?omsId={keys.omsId}",Method.POST);
            restRequest.AddJsonBody(JsonConvert.SerializeObject(request));
            var cancellationTokenSource = new CancellationTokenSource();
            return ApiClient().ExecuteAsync<CodeResult>(restRequest, cancellationTokenSource.Token);
        }

        public Task<IRestResponse<Ping>> PingServer(string omsId)
        {
            var restRequest = new RestRequest($"ping?omsId={keys.omsId}", Method.GET);
            var cancellationTokenSource = new CancellationTokenSource();
            return ApiClient().ExecuteAsync<Ping>(restRequest, cancellationTokenSource.Token);
        }


        public Task<IRestResponse<Subscription>> Subscription(string deviceSerial)
        {
            var restRequest = new RestRequest($"subscription/find_with_serial/", Method.GET);
            restRequest.AddParameter("serial", deviceSerial);
            restRequest.AddParameter("format", "json");
            var cancellationTokenSource = new CancellationTokenSource();
            return BillingClient().ExecuteAsync<Subscription>(restRequest, cancellationTokenSource.Token);
        }

        public Task<IRestResponse<Ping>> SendAggregationCodes(AggregationRequest request)
        {
            var restRequest = new RestRequest($"/aggregation?omsId={keys.omsId}", Method.POST);
            var requestJson = JsonConvert.SerializeObject(request);
            restRequest.AddJsonBody(requestJson);
            var cancellationTokenSource = new CancellationTokenSource();
            return ApiClient().ExecuteAsync<Ping>(restRequest, cancellationTokenSource.Token);
        }


        private RestClient ApiClient()
        {
            var restClient = new RestClient(BASE_URL);
            restClient.AddDefaultHeader("Accept", "application/json");
            restClient.AddDefaultHeader("clientToken", keys.clientToken);
            return restClient;
        }

        private RestClient BillingClient()
        {
            var restClient = new RestClient(BILLING_ENDPOINT);
            restClient.AddDefaultHeader("Content-Type", "application/json");
            restClient.AddDefaultHeader("Authorization", "Token d1d012233cfde482e88413eb5dc3a145ed4b0b6f");
            return restClient;
        }

  
    }
}
