﻿using CodeMarking.Entities;
using Marker.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Model.Server
{
    public interface IApi
    {
        Task<IRestResponse<Ping>> PingServer(string omsId);
        Task<IRestResponse<Order>> CreateOrder(OrderRequest request);
        Task<IRestResponse<OrderStatus>> CheckOrderStatus(OrderStatusRequest request);
        Task<IRestResponse<CodeResult>> GetOrderCodes(CodeRequest request);
        Task<IRestResponse<OrderCloseResult>> CloseOrder(Order order);
        Task<IRestResponse<CodeResult>> UtilisatonCode(Utilisation request);
        Task<IRestResponse<Ping>> SendAggregationCodes(AggregationRequest request);
        Task<IRestResponse<Subscription>> Subscription(string deviceSerial);
    }
}
