﻿using CodeMarking.Entities;
using FluentResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer.Discovery;

namespace CodeMarking.Tools.Printer.Zebra
{
    public class ZebraPrinterImpl : IPrinter
    {

        public Result PrintCodes(List<Codes> codes)
        {
            try
            {

                var printers = new List<String>();
              
                NetworkDiscoverer.FindPrinters(null, printers);

                List<DiscoveredPrinter> printerList = GetUSBPrinters();

                string zpl_string = $@"^XA
^ FO100,100
 ^ BXN,10,200
  ^ FDZEBRA TECHNOLOGIES CORPORATION
333 CORPORATE WOODS PARKWAY
VERNON HILLS, IL
60061 - 3109 ^ FS
^XZ";
                if (printerList.Count > 0)
                {
                    // in this case, we arbitrarily are printing to the first found printer  
                    DiscoveredPrinter discoveredPrinter = printerList[0];
                    Connection connection = discoveredPrinter.GetConnection();
                    connection.Open();
                    connection.Write(Encoding.UTF8.GetBytes(zpl_string));
                }
                else
                {
                    return Result.Fail("No printers found");
                }
                return Result.Ok();

            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public List<DiscoveredPrinter> GetUSBPrinters()
        {
            List<DiscoveredPrinter> printerList = new List<DiscoveredPrinter>();
            try
            {
                var usbPrinters = UsbDiscoverer.GetZebraUsbPrinters();
                var zebraPrinters = UsbDiscoverer.GetZebraUsbPrinters();


                foreach (DiscoveredUsbPrinter usbPrinter in UsbDiscoverer.GetZebraUsbPrinters())
                {
                    printerList.Add(usbPrinter);
                    Console.WriteLine(usbPrinter);
                }
            }
            catch (ConnectionException e)
            {
                Console.WriteLine($"Error discovering local printers: {e.Message}");
            }


            Console.WriteLine("Done discovering local printers.");
            return printerList;
        }
    }
}
