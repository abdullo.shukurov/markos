﻿using CodeMarking.Model.System;
using CodeMarking.Tools.Printer.Zebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Tools.Printer
{
    public class PrinterFactory
    {
        private IKeysHolder keys;
        GodexZebraImpl godexPrinter;
        ZebraPrinterImpl zebraPrinter;

        public PrinterFactory(IKeysHolder keys, GodexZebraImpl godexPrinter, ZebraPrinterImpl zebraPrinter)
        {
            this.keys = keys;
            this.godexPrinter = godexPrinter;
            this.zebraPrinter = zebraPrinter;
        }

        public IPrinter Printer => keys.PrinterType == PrinterTypes.GoDEX ? godexPrinter : (IPrinter)zebraPrinter;
    }
}
