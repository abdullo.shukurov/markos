﻿using CodeMarking.Entities;
using CodeMarking.Model.System;
using FluentResults;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer.Discovery;
using static CodeMarking.Tools.Printer.GoodexPrinter;

namespace CodeMarking.Tools.Printer
{
    public class GodexZebraImpl : IPrinter
    {
        private GodexPrinter printer;
        private IKeysHolder keys;
        public GodexZebraImpl(GodexPrinter Printer, IKeysHolder keys)
        {
            this.printer = Printer;
            this.keys = keys;
            Console.WriteLine(PrinterSettings.InstalledPrinters);
        }

        public Result PrintCodes(List<Codes> codes)
        {
            try
            {
                printer.Open("172.19.8.239", 9100);
                string printData = "";
                codes.ForEach(x =>
                {
                    printData += DataMatrixCode(x.code);
                });

                var result = EZioApi.sendcommand(printData);
                printer.Close();
                return Result.Ok();
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }

            /*     printer.Command.Start();

                 codes.ForEach(x =>
                 {
                     printer.Command.Start();
                     printer.Command.PrintDataMatrix(keys.XPrintCoordinate,keys.YPrintCoordinate,6,"0", "~1"+ x.code);
                     printer.Command.End();
                 });
                 printer.Close();
                 return Result.Ok();
             }
             catch (Exception ex)
             {
                 return Result.Fail(ex.ToString());
             }*/

        }

        private string DataMatrixCode(string code)
        {
            return $@"^XA
 ^FO{keys.XPrintCoordinate},
  {keys.YPrintCoordinate}
^BXN, ,200,0,0,1,~
^FH\
 ^FD\7E1{code}^FS
^PQ1,0,1,Y^XZ";
        }


        public class DiscoverHandler : DiscoveryHandler
        {
            void DiscoveryHandler.DiscoveryError(string message)
            {
                Console.WriteLine(message);
            }

            void DiscoveryHandler.DiscoveryFinished()
            {
                Console.WriteLine("Finished");
            }

            void DiscoveryHandler.FoundPrinter(DiscoveredPrinter printer)
            {
                Console.WriteLine(printer.Address);
            }
        }

    }
}
