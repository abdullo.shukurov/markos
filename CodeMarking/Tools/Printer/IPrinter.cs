﻿using CodeMarking.Entities;
using FluentResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Tools.Printer
{
    public interface IPrinter
    {
        Result PrintCodes(List<Codes> codes);
        
    }
}
