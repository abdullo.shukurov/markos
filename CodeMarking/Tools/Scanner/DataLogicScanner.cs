﻿using CodeMarking.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Tools.Scanner
{
    public class DataLogicScanner : BarcodeScanner
    {
        private TcpClient client;
        private BarcodeHandler handler;

        string CODE_PATTERN { get => "(00).{15,34}(\n)"; }
        byte[] data = new byte[256];
        StringBuilder response = new StringBuilder();


        void Init()
        {
            try
            {
                client = new TcpClient();
                client.Connect("172.19.8.108", 23);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        protected override void ReadData()
        {
            if (client == null)
            {
                Init();
            }

            try
            {
                NetworkStream stream = client.GetStream();
                int bytes = stream.Read(data, 0, data.Length);
                response.Append(Encoding.UTF8.GetString(data, 0, bytes));
                var results = response.ToString().Match(CODE_PATTERN)?.Select(x => x.TrimEnd())?.ToList();
                if (results.Count > 0)
                {
                    handler.Invoke(results);
                    response.Clear();
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }

        public void Stop()
        {
            try
            {
                client.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        public override void AddBarcodeHandler(BarcodeHandler handler)
        {
            this.handler += handler;
        }

        public override void RemoveBarcodeHandler(BarcodeHandler handler)
        {
            this.handler -= handler;
        }

    }
}
