﻿using CodeMarking.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CodeMarking.Tools.Scanner
{
    public class DataLogicAggregactionScanner : AggregationScanner
    {

        private TcpClient client;
        private BarcodeHandler handler;
        string BLOC_PATTERN { get => "(01).{386,450}"; }
        string CODE_PATTERN { get => "(01).{28,34}(;)"; }


        void Init()
        {
            client = new TcpClient();
            client.Connect("172.19.8.136", 51236);
        }

        protected override void ReadData()
        {
            try
            {
                if (client == null)
                {
                    Init();
                }

                byte[] data = new byte[512];
                StringBuilder response = new StringBuilder();
                NetworkStream stream = client.GetStream();
                int bytes = stream.Read(data, 0, data.Length);
                response.Append(Encoding.UTF8.GetString(data, 0, bytes));
                var results = response.ToString().Match(BLOC_PATTERN);

                if (results.Count > 0)
                {
                    results.ForEach(x =>
                    {
                        x = x + ";";
                        var codes = x.Match(CODE_PATTERN).Select(code => code.TrimEnd(';')).ToList();
                        var i = 0;
                        codes.ForEach(code => Console.WriteLine(i++ + " " + code));
                        handler.Invoke(codes);
                    });
                    response.Clear();
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }



        public override void AddBarcodeHandler(BarcodeHandler handler)
        {
            this.handler += handler;
        }

        public override void RemoveBarcodeHandler(BarcodeHandler handler)
        {
            this.handler -= handler;
        }


    }
}
