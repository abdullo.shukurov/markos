﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Timers;

namespace CodeMarking.Tools.Scanner
{
    public abstract class BarcodeScanner
    {
        protected Timer timer;
        protected int tickmeasuring = 100;
        protected bool IsRunning = false;

        public abstract void AddBarcodeHandler(BarcodeHandler handler);
        public abstract void RemoveBarcodeHandler(BarcodeHandler handler);
        protected abstract void ReadData();

        public void Start() {

            if (IsRunning)
                return;

            IsRunning = true;
           
            timer = new Timer
            {
                Interval = tickmeasuring
            };
            timer.Elapsed += (sd, _) =>
            {
                ReadData();
            };
            timer.Start();
        }

        public void Stop() {
            IsRunning = false;
            timer.Stop();
        }
    }
}
