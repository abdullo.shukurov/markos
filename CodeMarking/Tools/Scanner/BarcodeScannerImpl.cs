﻿using CodeMarking.Extensions;
using GlobalLowLevelHooks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static GlobalLowLevelHooks.KeyboardHook;

namespace CodeMarking.Tools.Scanner
{
    public class BarcodeScannerImpl : BarcodeScanner
    {
        private KeyboardHook keyboardHook;
        private Queue myQ;
        private BarcodeHandler handler;


        public override void AddBarcodeHandler(BarcodeHandler handler)
        {
            this.handler += handler;
        }

        public override void RemoveBarcodeHandler(BarcodeHandler handler)
        {
            this.handler -= handler;
        }

        protected override void ReadData()
        {
        }

        private void InitKeyboardHook()
        {
            keyboardHook = new KeyboardHook();
            keyboardHook.KeyUp += new KeyboardHook.KeyboardHookCallback(OnKeyUp);
            keyboardHook.Install();
            myQ = new Queue();
        }


        private void OnKeyUp(KeyboardHook.VKeys key)
        {
            string keyboardText = "";
            if (key == VKeys.OEM_MINUS)
                keyboardText = "-";
            else
                keyboardText = key.ToString().Substring(key.ToString().Count() - 1, 1);

            myQ.Enqueue(keyboardText);
            keyboardText = "";
            foreach (var item in myQ)
            {
                keyboardText += item.ToString();
            }
            if (myQ.Count > 31)
            {
              
            }
        }
    }
}
