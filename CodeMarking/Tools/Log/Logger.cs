﻿using FluentResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Tools.Log
{
   public class Logger
    {
        public delegate void LogHandler(string title, string log);
        public event LogHandler LogAction;

        public void WriteLog(string title, ResultBase result)
        {
            if (result.IsSuccess)
                LogAction?.Invoke(title, "Успешно");
            else
                LogAction?.Invoke(title, $"Ошибка {string.Join("\n", result.Errors.Select(x => x.Message))}");
        }
    }
}
