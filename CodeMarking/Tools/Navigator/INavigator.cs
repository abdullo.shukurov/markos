﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CodeMarking.Tools.Navigator
{
    public interface INavigator
    {
        void InitNavigationService(NavigationService service);
        void Forward(Page page);
        void Back();
        void Replace(Page page);
    }
}
