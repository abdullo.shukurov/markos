﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CodeMarking.Tools.Navigator
{
   public class NavigatorImpl : INavigator
    {
        NavigationService navService;

        public void InitNavigationService(NavigationService service)
        {
            this.navService = service;
        }

        public void Back()
        {
            if (navService == null)
                throw new NavigatorNotInitalizedException();
            navService?.GoBack();
        }

        public void Forward(Page page)
        {
            if (navService == null)
                throw new NavigatorNotInitalizedException();
            navService?.Navigate(page);
        }

        public void Replace(Page page)
        {
            if (navService == null)
                throw new NavigatorNotInitalizedException();


            while (navService.CanGoBack)
                navService.GoBack();

            navService.Navigate(page);
        }

        public void Dispose()
        {
            navService = null;
        }
    }

    public class NavigatorNotInitalizedException : ArgumentNullException
    {
        public NavigatorNotInitalizedException() { }
        public NavigatorNotInitalizedException(string message)
            : base($"Navigator not initialized {message}") { }
    }
}
