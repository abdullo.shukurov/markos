﻿using CodeMarking.Model.Server;
using CodeMarking.Model.System;
using CodeMarking.Tools.Log;
using CodeMarking.Tools.Navigator;
using CodeMarking.Tools.Printer;
using CodeMarking.Tools.Scanner;
using DevExpress.Mvvm;
using DevExpress.Mvvm.UI;
using GKeys;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CodeMarking.Tools.Printer.GoodexPrinter;

namespace CodeMarking.DI.Modules
{
    public class MainModule : NinjectModule
    {
        public override void Load()
        {
            Bind<GYek>().ToSelf().InSingletonScope();
            Bind<IKeysHolder>().To<KeyHolderImpl>().InSingletonScope();
            Bind<IMessageBoxService>().To<MessageBoxService>().InSingletonScope();
            Bind<IResourceManager>().To<ResourceManager>().InSingletonScope();
            Bind<IOpenFileDialogService>().To<OpenFileDialogService>().InSingletonScope();
            Bind<INavigator>().To<NavigatorImpl>().InSingletonScope();
            Bind<IApi>().To<ApiImpl>().InSingletonScope();
            Bind<GodexPrinter>().ToSelf().InSingletonScope();
            Bind<Logger>().ToSelf().InSingletonScope();
            Bind<IPrinter>().To<GodexZebraImpl>().InSingletonScope();
            Bind<BarcodeScanner>().To<DataLogicScanner>().InSingletonScope();
            Bind<AggregationScanner>().To<DataLogicAggregactionScanner>().InSingletonScope();
        }
    }
}
