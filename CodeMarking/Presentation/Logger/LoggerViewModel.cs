﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static CodeMarking.Tools.Log.Logger;

namespace CodeMarking.Presentation.Logger
{
   public  class LoggerViewModel
    {
        private readonly Tools.Log.Logger logger;
        LogHandler logHandler;
        private SynchronizationContext uiContext = SynchronizationContext.Current;
        public virtual string Logs { get; set; }

        [Inject]
        public LoggerViewModel(Tools.Log.Logger logger)
        {
            this.logger = logger;
            InitLogHandler();
        }

        private void InitLogHandler()
        {
            logHandler += AddLog;
            logger.LogAction += logHandler;
        }

        private void AddLog(string title, string text)
        {
            uiContext.Send((x) => Logs += title + Environment.NewLine + text + Environment.NewLine, null);
        }

        public void OnStop()
        {
            logger.LogAction -= logHandler;
            logHandler = null;
        }
    }
}
