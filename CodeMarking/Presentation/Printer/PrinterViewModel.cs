﻿using CodeMarking.Entities;
using CodeMarking.Extensions;
using CodeMarking.Model.Interactor.Printer;
using DevExpress.Mvvm;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CodeMarking.Presentation.Printer
{
    public class PrinterViewModel : ViewModelBase
    {
      private   PrinterInteractor printerInteractor;
        private IMessageBoxService messageDialog;
        public int Count { get; set; }
        public string Code { get; set; }
        public List<string> CisTypes { get => new List<string> { CIS_TYPE.UNIT, CIS_TYPE.GROUUP }; }
        public string CisType { get; set; }
        public string PrinterIpAddress { get; set; }

        [Inject]
        public PrinterViewModel(PrinterInteractor printerInteractor, IMessageBoxService messageDialog)
        {
            this.printerInteractor = printerInteractor;
            this.messageDialog = messageDialog;
        }


        public ICommand OnPrintClicked
        {
            get => new DelegateCommand(async () =>
            {
                if (Count > 0 && !string.IsNullOrEmpty(Code))
                {
                    var result =await  printerInteractor.PrintCodes(Code, Count,CisType);
                    if (result.IsSuccess)
                    {
                        messageDialog.ShowMessage("Успешно");
                    }
                    else messageDialog.ShowMessage("Ошибка:\n" + result.Errors.ToUserMessage());
                }
            });
        }
    }
}
