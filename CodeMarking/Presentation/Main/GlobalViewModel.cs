﻿using CodeMarking.Model.Interactor.Sync;
using CodeMarking.Tools.Navigator;
using CodeMarking.UI.Auth;
using CodeMarking.UI.Printer;
using DevExpress.Mvvm;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CodeMarking.Presentation.Main
{
    class GlobalViewModel : ViewModelBase, IDisposable
    {
        INavigator navigation;
        SyncInteractor syncInteractor;
        private int SYNC_INTERVAL = 10 * 1000;
        private bool IsSyncing = false;
        Timer syncTimer;

        [Inject]
        public GlobalViewModel(INavigator navigation, SyncInteractor syncInteractor)
        {
            this.navigation = navigation;
            this.syncInteractor = syncInteractor;
            InitSyncTimer();
            navigation.Forward(new AuthPage());
        }

        private void InitSyncTimer()
        {
            syncTimer = new Timer
            {
                Interval = SYNC_INTERVAL
            };
            syncTimer.Elapsed += async (sender, args) =>
            {
                if (!IsSyncing)
                {
                    IsSyncing = true;
                    await SyncAll();
                    IsSyncing = false;
                }
            };
            syncTimer.Start();
        }


        private async Task SyncAll()
        {
            await syncInteractor.SyncBilling();
            await syncInteractor.SyncScannedCodes();
        }

        public void Dispose()
        {
            syncTimer?.Stop();
            syncTimer?.Dispose();
        }
    }
}