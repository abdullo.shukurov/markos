﻿using CodeMarking.Entities;
using CodeMarking.Model.Interactor.Main;
using CodeMarking.UI.Log;
using CodeMarking.UI.Order;
using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CodeMarking.Presentation.Main
{
    public class MainViewModel : ViewModelBase
    {
        public MenuInteractor menuInteractor;
        public List<MenuItem> Menus { get; set; }
        public int SelectedMenu { get; set; }
        public MenuInteractor.ChangePageDelegate changePageDelegate;

        public MainViewModel(MenuInteractor menuInteractor)
        {
            this.menuInteractor = menuInteractor;
            Init();
        }

        public void Init()
        {
            Menus = menuInteractor.MenuItems();
            changePageDelegate += ChangePage;
            menuInteractor.OnChangePage += changePageDelegate;
        }

        private void ChangePage(string title, object data)
        {
            var menu = Menus.FirstOrDefault(x => x.Title == MenuInteractor.PAGE_ORDER);
            if (menu == null) return;
            var page = menu.Page as OrderListPage;
            SelectedMenu = Menus.IndexOf(menu);
        }

        public void Dispose()
        {

        }

        public ICommand OnLogsClicked
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    new LogsDialog().Show();
                });
            }
        }

    }
}
