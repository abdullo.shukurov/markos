﻿using CodeMarking.Model.Interactor.Scanner;
using CodeMarking.Tools.Scanner;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Marker.Presentation.Scanner
{
    public class AutoScannerViewModel
    {
        private BarcodeScanner scanner;
        private ScannerInteractor scannerInteractor;
        public List<Barcode> codes
        {
            get
            {
                int i = 0;
                return barcodes.Select(x => new Barcode() { id = i, code = x, status = false })
                    .OrderByDescending(x => x.id).ToList();
            }
        }
        private SynchronizationContext uiContext = SynchronizationContext.Current;
        BarcodeHandler codesHandler;
        List<string> barcodes { get; set; } = new List<string>();
        [Inject]
        public AutoScannerViewModel(BarcodeScanner scanner, ScannerInteractor scannerInteractor)
        {
            this.scanner = scanner;
            this.scannerInteractor = scannerInteractor;
            codesHandler += OnCodesAdded;
            scanner.AddBarcodeHandler(codesHandler);
            scanner.Start();
        }

        private async void OnCodesAdded(List<string> codes)
        {
            if (codes != barcodes)
            {
                var result = await scannerInteractor.AddScannedCodes(codes);
                if (result.IsSuccess)
                    uiContext.Send((x) => { barcodes.AddRange(codes); }, null);
            }
        }
    }

    public class Barcode
    {
        public int id { get; set; }
        public string code { get; set; }
        public bool status { get; set; }
    }
}
