﻿using CodeMarking.Model.Interactor.Scanner;
using DevExpress.Mvvm;
using Ninject;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Presentation.Scanner
{
    public class ScannerViewModel : ViewModelBase
    {
        private ScannerInteractor scannerInteractor;
        public List<string> Codes { get; set; } = new List<string>();
        private List<string> _Codes { get; set; } = new List<string>();
        public string Code
        {
            set
            {
                _Code = value;
            }
            get => _Code;
        }

        public Color StatusColor { get; set; }
        public string StatusText { get; set; }
        private string _Code { get; set; }

        [Inject]
        public ScannerViewModel(ScannerInteractor scannerInteractor)
        {
            this.scannerInteractor = scannerInteractor;
        }

        public void TextChange(string text)
        {
            Code = text;
            OnCodeChange();
        }
        private async void OnCodeChange()
        {
            if (!_Code.StartsWith("01"))
            {
                Code = "";
                StatusColor = Color.Red;
                StatusText = "Ошибка";
            }
            else
            {
                _Code = _Code.Insert(25, "");
                var result = await scannerInteractor.AddScannedCode(_Code);
                StatusColor = result.IsSuccess ? Color.Green : Color.Red;
                StatusText = result.IsSuccess ? "Успешно" : "Ошибка";
                if (result.IsSuccess)
                {
                    Codes.Add(Code);
                    Code = "";
                }
            }
        }
    }
}
