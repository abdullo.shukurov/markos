﻿using CodeMarking.Entities;
using CodeMarking.Model.Interactor.Scanner;
using CodeMarking.Tools.Scanner;
using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CodeMarking.Presentation.Scanner
{
    public class AggregationViewModel : ViewModelBase, IDisposable
    {
        public List<string> Codes { get; set; }
        public string AggregationCode { get; set; }
        public List<Aggregation> AggregationCodes { get; set; } = new List<Aggregation>();
        private AggregationScanner aggregationScanner;
        AggregationInteractor aggregationInteractor;
        private BarcodeScanner codesScanner;
        private SynchronizationContext uiContext = SynchronizationContext.Current;
        BarcodeHandler aggregationCodeHandler;
        BarcodeHandler codesHandler;

        public AggregationViewModel(AggregationScanner aggregationScanner, BarcodeScanner codesScanner, AggregationInteractor aggregationInteractor)
        {
            this.aggregationScanner = aggregationScanner;
            this.codesScanner = codesScanner;
            this.aggregationInteractor = aggregationInteractor;
            aggregationCodeHandler += OnAggregationCode;
            codesHandler += OnCodesAdded;
            aggregationScanner.AddBarcodeHandler(aggregationCodeHandler);
            codesScanner.AddBarcodeHandler(codesHandler);
            aggregationScanner.Start();
            codesScanner.Start();
        }


        private void OnCodesAdded(List<string> barcodes)
        {
            if (Codes != barcodes)
                uiContext.Send((x) => { AggregationCode = barcodes.LastOrDefault(); CheckAggregation(); }, null);
        }

        public void OnAggregationCode(List<string> codes)
        {
            if (AggregationCode != codes.LastOrDefault())
                uiContext.Send((x) => { 
                    Codes = codes; CheckAggregation();
                }, null);
        }

        private async void CheckAggregation()
        {
            if (Codes?.Count == 12 && AggregationCode?.Length>15)
            {
                var aggregation = new Aggregation()
                {
                    aggregatedItemsCount = Codes.Count,
                    aggregationType = AGGREGATION_TYPE.AGGREGATION,
                    unitSerialNumber = AggregationCode.Substring(2),
                    sntins = Codes,
                    aggregationUnitCapacity = Codes.Count,
                };
                var save = await aggregationInteractor.AddAggregation(aggregation);
                if (save.IsSuccess)
                {
                    AggregationCodes.Add(aggregation);
                    AggregationCodes = AggregationCodes.OrderByDescending(x=>x.id).ToList();
                    ClearCodes();
                }
            }
        }


        private void ClearCodes()
        {
           
        }

        public void Dispose()
        {
            codesScanner.RemoveBarcodeHandler(codesHandler);
            aggregationScanner.RemoveBarcodeHandler(aggregationCodeHandler);
            codesHandler = null;
            aggregationCodeHandler = null;
        }
    }
}
