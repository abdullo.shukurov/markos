﻿using CodeMarking.Model.Interactor.Auth;
using CodeMarking.Model.System;
using CodeMarking.Tools.Navigator;
using CodeMarking.UI.Main;
using DevExpress.Mvvm;
using System.Windows.Controls;
using System.Windows.Input;
using CodeMarking.Extensions;
using CodeMarking.Entities;
using CodeMarking.UI.Printer;
using CodeMarking.UI.Scanner;
using CodeMarking.UI.Settings;

namespace CodeMarking.Presentation.Auth
{
    class AuthViewModel : ViewModelBase
    {
        INavigator navigation;
        AuthInteractor authInteractor;
        IMessageBoxService messageService;
        IResourceManager resourceManager;



        public AuthViewModel(INavigator navigation,
            IMessageBoxService messageService,
            IResourceManager resourceManager,
            AuthInteractor authInteractor)
        {
            this.navigation = navigation;
            this.authInteractor = authInteractor;
            this.messageService = messageService;
            this.resourceManager = resourceManager;
        }

        public ICommand OnAuthClick
        {
            get => new DelegateCommand<object>(async (parametr) =>
            {
                var passwordBox = parametr as PasswordBox;
                var password = passwordBox.Password;

                var result = await authInteractor.Auth(password);
                if (result.IsSuccess)
                {
                    var role = result.ValueOrDefault.role;
                    if (role == USER_ROLES.ROLE_ADMIN)
                        navigation.Replace(new MainPage());
                    else if (role == USER_ROLES.ROLE_PRINTER)
                        navigation.Replace(new PrinterPage());
                    else if (role == USER_ROLES.ROLE_SCANNER)
                        navigation.Replace(new ScannerPage());
                }
                else
                    messageService.ShowMessage(result.Errors.ToUserMessage());
            });
        }

        public ICommand OnSettingsClicked
        {
            get => new DelegateCommand(() =>
            {
                navigation.Forward(new SettingsPage());
            });
        }
    }
}
