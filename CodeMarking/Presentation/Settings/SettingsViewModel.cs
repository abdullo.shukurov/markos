﻿using CodeMarking.Entities;
using CodeMarking.Model.System;
using CodeMarking.Tools.Navigator;
using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CodeMarking.Presentation.Settings
{
    public class SettingsViewModel : ViewModelBase
    {
        public int XCordinate { get => keys.XPrintCoordinate; set => keys.XPrintCoordinate = value; }
        public int YCordinate { get => keys.YPrintCoordinate; set => keys.YPrintCoordinate = value; }
        public string Token { get => keys.clientToken; set => keys.clientToken = value; }
        public string OmsId { get => keys.omsId; set => keys.omsId = value; }
        public string SelectedPrinter { get => keys.PrinterType; set => keys.PrinterType = value; }
        public string SelectedExtension { get => keys.ProductExtension; set => keys.ProductExtension = value; }

        public List<string> PrinterTypes => new List<string> { Tools.Printer.PrinterTypes.GoDEX, Tools.Printer.PrinterTypes.Zebra };
        public List<string> ProductExtensions => new List<String> { PRODUCT_EXTENSION.ALCOHOL, PRODUCT_EXTENSION.BEER };

        private IKeysHolder keys;
        private IMessageBoxService messageDialog;
        INavigator navigation;

        public SettingsViewModel(IKeysHolder keys, IMessageBoxService messageDialog, INavigator navigation)
        {
            this.messageDialog = messageDialog;
            this.keys = keys;
            this.navigation = navigation;
        }

        public ICommand OnSaveClick
        {
            get => new DelegateCommand(() =>
           {
               if (keys.Save())
               {
                   messageDialog.ShowMessage($"Сохранено успешно");
                   navigation.Back();
               }
               else
                   messageDialog.ShowMessage($"Ошибка");
           });
        }

        public ICommand OnCancelClick
        {
            get => new DelegateCommand(() =>
            {
                navigation.Back();
            });
        }
    }
}
