﻿using CodeMarking.Entities;
using CodeMarking.Extensions;
using CodeMarking.Model.Interactor.Order;
using CodeMarking.Model.Interactor.Printer;
using CodeMarking.Model.System;
using CodeMarking.Tools.Printer;
using CodeMarking.UI.Order;
using CodeMarking.UI.Tools;
using DevExpress.Mvvm;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CodeMarking.Presentation.Order
{
    public class OrderListViewModel : ViewModelBase
    {

        private IResourceManager resourceManager;
        private OrderListInteractor orderListinteractor;
        private PrinterInteractor printerInteractor;
        private IMessageBoxService messageDialog;
        private IPrinter printer;
        public List<Entities.Order> Orders { get; set; }
        public Entities.Order SelectedOrder { get; set; }


        [Inject]
        public OrderListViewModel(IResourceManager resourceManager, IMessageBoxService messageDialog,
            OrderListInteractor interactor, PrinterInteractor printerInteractor)
        {
            this.resourceManager = resourceManager;
            this.orderListinteractor = interactor;
            this.messageDialog = messageDialog;
            this.printerInteractor = printerInteractor;
            RefreshList();
        }

        private async void RefreshList()
        {
            var result = await orderListinteractor.AllOders();
            if (result.IsSuccess) Orders = result.ValueOrDefault;
            else messageDialog.ShowMessage(result.Errors.ToUserMessage());
        }

        internal async void OnCloseOrderClicked()
        {
            if (SelectedOrder == null) return;
            var result = await orderListinteractor.CloseOrder(SelectedOrder);
            if (result.IsSuccess)
            {
                messageDialog.ShowMessage("Успешно");
                RefreshList();
            }
            else messageDialog.ShowMessage("Ошибка:\n" + result.Errors.ToUserMessage());
        }

        internal async void GetBarcodesClicked()
        {
            if (SelectedOrder == null) return;
            if (SelectedOrder.status != ORDER_STATUS.ACTIVE)
            {
                messageDialog.ShowMessage($"Статус должен быть {ORDER_STATUS.ACTIVE}");
                return;
            }
            var counterDialog = new CounterDialog("Получить штрих кодов ");
            counterDialog.ShowDialog();
            if (counterDialog.DialogResult != true) return;

            var request = new CodeRequest()
            {
                orderId = SelectedOrder?.orderId,
                lastBlockId = SelectedOrder?.lastBlockId,
                omsId = SelectedOrder?.omsId,
                gtin = SelectedOrder?.gtin,
                cisType = SelectedOrder?.cisType,
                quantity = counterDialog.Count
            };
            var result = await orderListinteractor.GetOrderCodes(request);
            if (result.IsSuccess)
            {
                messageDialog.ShowMessage($"Получено {result.ValueOrDefault?.codes?.Count} штрих-кодов");
                RefreshList();
            }
            else
                messageDialog.ShowMessage("ОШИБКА:\n" + result.Errors.ToUserMessage());
        }

        internal async void OnCheckStatusClicked()
        {
            if (SelectedOrder == null) return;
            var statusRequest = new OrderStatusRequest()
            {
                order = SelectedOrder,
                gtin = SelectedOrder.gtin
            };
            var result = await orderListinteractor.CheckStatus(statusRequest);
            if (result.IsSuccess)
            {
                var message = "СТАТУС: " + result.ValueOrDefault?.bufferStatus;
                if (!string.IsNullOrEmpty(result.ValueOrDefault?.rejectionReason))
                    message += $"\nПричина отказа: {result.ValueOrDefault?.rejectionReason}";
                messageDialog.ShowMessage(message);
                RefreshList();
            }
            else
                messageDialog.ShowMessage("ОШИБКА:\n" + result.Errors.ToUserMessage());
        }

        internal async void PrintCodes()
        {
            if (SelectedOrder == null) return;
            var counterDialog = new CounterDialog("Печать штрих кодов");
            counterDialog.ShowDialog();
            if (counterDialog.DialogResult != true) return;
            var result = await printerInteractor.PrintCodes(SelectedOrder, counterDialog.Count);
            if (result.IsSuccess)
            {
                messageDialog.ShowMessage("Успешно");
                RefreshList();
            }
            else messageDialog.ShowMessage("Ошибка:\n" + result.Errors.ToUserMessage());
        }

        public ICommand OnCreateClicked
        {
            get => new DelegateCommand(() =>
            {
                var result = new CreateOrderDialog().ShowDialog();
                if (result == true)
                {
                    RefreshList();
                }
            });
        }
    }
}
