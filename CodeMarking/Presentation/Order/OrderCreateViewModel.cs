﻿using CodeMarking.Entities;
using CodeMarking.Extensions;
using CodeMarking.Model.Interactor.Order;
using CodeMarking.Model.System;
using DevExpress.Mvvm;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CodeMarking.Presentation.Order
{
    class OrderCreateViewModel : ViewModelBase
    {

        public string ButtonText { get; set; }
        public string ProductBarcode { get; set; }
        public string ProductName { get; set; }
        public string ContactName { get; set; }
        public int ProductCount { get; set; }

        public string CisType { get; set; }

        public List<string> CisTypes { get => new List<string>() { CIS_TYPE.UNIT, CIS_TYPE.GROUUP }; }

        private IResourceManager resourceManager;
        private OrderCreateInteractor interactor;
        private IMessageBoxService messageDialog;
        public event EventHandler OnRequestClose;


        [Inject]
        public OrderCreateViewModel(IResourceManager resourceManager, IMessageBoxService messageDialog, OrderCreateInteractor interactor)
        {
            this.resourceManager = resourceManager;
            this.interactor = interactor;
            this.messageDialog = messageDialog;
            ButtonText = resourceManager.GetStringRes("titleCreate");
        }

        public ICommand OncreateClicked
        {
            get => new DelegateCommand(async () =>
            {
                if (string.IsNullOrEmpty(ProductBarcode) || ProductCount <= 0)
                {
                    messageDialog.ShowMessage("Пожалуйста внесите все данные");
                    return;
                }
                var orderProduct = new OrderProduct()
                {
                    name = ProductName,
                    gtin = ProductBarcode,
                    cisType = CisType,
                    quantity = ProductCount,
                    templateId = interactor.TemplateId(CisType),
                    serialNumberType = PRODUCT_SERIAL_TYPE.OPERATOR

                };
                var orderRequest = new OrderRequest()
                {
                    products = new List<OrderProduct> { orderProduct },
                    contactPerson = ContactName,
                    createMethodType = ORDER_CREATE_METHOD_TYPE.SELF_MADE,
                    releaseMethodType = ORDER_RELEASE_TYPE.PRODUCTION,
                };

                var result = await interactor.CreateOrder(orderRequest);
                if (result.IsSuccess) { 
                    messageDialog.ShowMessage($"Успешно создано: {result.Value.orderId}");
                    OnRequestClose(this, null);
                }      
                else
                    messageDialog.ShowMessage($"Ошибка при создании заказа: {result.Errors.ToUserMessage()}");
            });
        }

    }
}
