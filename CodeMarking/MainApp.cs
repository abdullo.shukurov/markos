﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking
{
    public class MainApp
    {
        /// <summary>
        /// The current application instance
        /// </summary>
        private static MainApp _current;
        public IKernel Container { get; set; }

        private MainApp() { }

        public static MainApp Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new MainApp();
                }

                return _current;
            }
        }
    }
}
