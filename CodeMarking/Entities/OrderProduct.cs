﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Entities
{
    public class OrderProduct
    {
        public long id { get; set; }
        public string name { get; set; }
        public string gtin { get; set; }
        public int quantity { get; set; }
        public int templateId { get; set; }
        public string serialNumberType { get; set; }
        public string serialNumbers { get; set; }
        public string cisType { get; set; }

    }

    public static class PRODUCT_EXTENSION {
        public static string ALCOHOL = "alcohol";
        public static string BEER = "beer";
    }

    public static class PRODUCT_SERIAL_TYPE
    {
        public static string OPERATOR = "OPERATOR";
    }

    public static class CIS_TYPE {
        public static string GROUUP = "GROUP";
        public static string UNIT = "UNIT";
    }
}
