﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Entities
{
    public class ServerErrors
    {
        public List<ServerError> fieldErrors { get; set; } = new List<ServerError>();
        public List<string> globalErrors { get; set; }
        public bool success { get; set; }
    }

    public class ServerError
    {
        public string fieldError { get; set; }
        public string fieldName { get; set; }
    }
}
