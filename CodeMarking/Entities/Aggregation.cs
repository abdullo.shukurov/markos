﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Entities
{
    public class AggregationRequest
    {
        public string participantId { get; set; }
        public List<Aggregation> aggregationUnits { get; set; }
    }


    public class Aggregation { 
        public int id { get; set; }
        public int aggregatedItemsCount { get; set; }
        public string aggregationType { get; set; }
        public int aggregationUnitCapacity { get; set; }
        public List<string> sntins { get; set; }
        public string sntinJson { get; set; }
        public string unitSerialNumber { get; set; }

        public int isSynced { get; set; }
    }

    public static class AGGREGATION_TYPE {
        public static string AGGREGATION = "AGGREGATION";
        public static string UPDATE = "UPDATE";
    }
}
