﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Entities
{
    public class Codes
    {
        public int id { get; set; }
        public string code { get; set; }
        public string orderId { get; set; }
        public string gtin { get; set; }
        public string blockId { get; set; }

        public string cisType { get; set; }
        public int isPrinted { get; set; } = 0;
    }

    public class ScannedCodes {
        public int id { get; set; }
        public string code { get; set; }
        public int isSynced { get; set; }
        public string dateStr { get; set;}
    }

    public class CodeResult
    {
        public string omsId { get; set; }
        public List<string> codes { get; set; }
        public String blockId { get; set; }
        public string orderId { get; set; }
    }

    public class CodeRequest
    {
        public string orderId { get; set; }
        public string omsId { get; set; }
        public string cisType { get; set; }

        public string gtin { get; set; }
        public int quantity { get; set; }
        public string lastBlockId { get; set; }
    }
}
