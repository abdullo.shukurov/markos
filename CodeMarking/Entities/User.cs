﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Entities
{
   public class User
    {

        public int id { get; set; }
        public string role { get; set; }
        public string name { get; set; }
        public string password { get; set; }
     }

    public static class USER_ROLES {
        public static string ROLE_ADMIN = "admin";
        public static string ROLE_ADMIN_NEW = "admin_new";
        public static string ROLE_SCANNER = "scanner";
        public static string ROLE_PRINTER = "printer";
    }
}
