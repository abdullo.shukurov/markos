﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Entities
{
    public class Utilisation
    {
        public List<string> sntins { get; set; }
        public string usageType { get; set; }
        public string productionLineId { get; set; }
        public string productionOrderId { get; set; }
        public string brandcode { get; set; }
        public string sourceReportId { get; set; }
    }
}
