﻿using CodeMarking.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace CodeMarking.Entities
{
    public class EntityContext : DbContext
    {
        public EntityContext() : base("DefaultConnection")
        {
            Database.SetInitializer<EntityContext>(null);
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<Codes> Codes { get; set; }
        public DbSet<User> User { get; set; }

        public DbSet<Aggregation> Aggregation { get; set; }

        public DbSet<ScannedCodes> ScannedCodes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}
