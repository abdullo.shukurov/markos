﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CodeMarking.Entities
{
   public  class MenuItem
    {
        public string Title { get; set; }
        public Page Page { get; set; }
        public Grid Icon { get; set; }
    }
}
