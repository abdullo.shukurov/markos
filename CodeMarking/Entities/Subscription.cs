﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marker.Entities
{
  public  class Subscription
    {
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string device_serial_number { get; set; }
    }
}
