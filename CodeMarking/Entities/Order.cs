﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMarking.Entities
{
    public static class ORDER_STATUS
    {
        public static string CREATED = "CREATED";
        public static string PENDING = "PENDING";
        public static string DECLINED = "DECLINED";
        public static string ACTIVE = "ACTIVE";
        public static string APPROVED = "APPROVED";
        public static string READY = "READY";
        public static string CLOSED = "CLOSED";
    }


    public static class ORDER_RELEASE_TYPE
    {
        public static string PRODUCTION = "PRODUCTION";
    }

    public static class ORDER_CREATE_METHOD_TYPE
    {
        public static string SELF_MADE = "SELF_MADE";
    }

    public class Order
    {
        public int id { get; set; }
        public string orderId { get; set; }
        public string omsId { get; set; }
        public int? expectedCompleteTimestamp { get; set; }
        public string lastBlockId { get; set; }
        public string status { get; set; }
        public int? maxCount { get; set; }
        public string gtin { get; set; }
        public string cisType { get; set; }
        public string name { get; set; }
        public int? saved { get; set; } = 0;
        public int? printed { get; set; } = 0;
        public int? scanned { get; set; } = 0;
    }

    public class OrderCreateRequest
    {
        public List<OrderProduct> products { get; set; }
        public string contactPerson { get; set; }
        public string releaseMethodType { get; set; }
        public string createMethodType { get; set; }
        public string productionOrderId { get; set; }

    }

    public class OrderRequest
    {
        public List<OrderProduct> products { get; set; }
        public string contactPerson { get; set; }
        public string releaseMethodType { get; set; }
        public string productionOrderId { get; set; }
        public string createMethodType { get; set; }

    }

    public class OrderStatusRequest
    {
        public Order order { get; set; }
        public string gtin { get; set; }
    }

    public class OrderStatus
    {
        public int availableCodes { get; set; }
        public string bufferStatus { get; set; }
        public string gtin { get; set; }
        public int leftInBuffer { get; set; }
        public string omsId { get; set; }
        public string orderId { get; set; }
        public bool poolsExhausted { get; set; }
        public string rejectionReason { get; set; }
        public int totalCodes { get; set; }
        public int totalPassed { get; set; }
        public int unavailableCodes { get; set; }
    }

    public class OrderCloseResult
    {
     public string omsId { get; set; }

    }

}
