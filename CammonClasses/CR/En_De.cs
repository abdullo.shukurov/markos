﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CammonClasses.CR
{
   public class En_De
    {
        public static string En(string input, string password)
        {
            byte[] clearBytes = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] encryptedData = En(clearBytes, password);
            return Convert.ToBase64String(encryptedData);
        }

        public static byte[] En(byte[] input, string password)
        {
            return En(input, Encoding.UTF8.GetBytes(password));
        }

        public static byte[] En(byte[] input, byte[] password)
        {
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            return En(input, pdb.GetBytes(32), pdb.GetBytes(16));
        }

        public static string De(string input, string password)
        {
            byte[] cipherBytes = Convert.FromBase64String(input);
            byte[] decryptedData = De(cipherBytes, password);
            return System.Text.Encoding.UTF8.GetString(decryptedData);
        }

        public static byte[] De(byte[] input, string password)
        {
            return De(input, Encoding.UTF8.GetBytes(password));
        }

        public static byte[] De(byte[] input, byte[] password)
        {
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            return De(input, pdb.GetBytes(32), pdb.GetBytes(16));
        }

        static byte[] En(byte[] clearData, byte[] Key, byte[] IV)
        {
            byte[] encryptedData = null;
            using (MemoryStream ms = new MemoryStream())
            {
                using (Rijndael alg = Rijndael.Create())
                {
                    alg.Key = Key;
                    alg.IV = IV;
                    using (CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearData, 0, clearData.Length);
                        cs.Close();
                    }
                    encryptedData = ms.ToArray();
                }
            }
            return encryptedData;
        }

        static byte[] De(byte[] cipherData, byte[] Key, byte[] IV)
        {
            byte[] decryptedData = null;
            using (MemoryStream ms = new MemoryStream())
            {
                using (Rijndael alg = Rijndael.Create())
                {
                    alg.Key = Key;
                    alg.IV = IV;
                    using (CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherData, 0, cipherData.Length);
                        cs.Close();
                    }
                    decryptedData = ms.ToArray();
                }
            }
            return decryptedData;
        }
    }
}
