﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer.Discovery;

namespace CodeMarkingTests.Tools.Printer
{
    [TestClass()]
  public  class PrinterTest
    {

        [TestInitialize()]
        public void Initilialize()
        {
          
        }


        [TestMethod()]
        public void AllOrdersTest()
        {
            try
            {
                foreach (DiscoveredPrinterDriver printer in UsbDiscoverer.GetZebraDriverPrinters())
                {
                    Console.WriteLine(printer);
                }

                foreach (DiscoveredUsbPrinter usbPrinter in UsbDiscoverer.GetZebraUsbPrinters(new ZebraPrinterFilter()))
                {
                    Console.WriteLine(usbPrinter);
                }
            }
            catch (ConnectionException e)
            {
                Console.WriteLine($"Error discovering local printers: {e.Message}");
            }
            Console.WriteLine("Done discovering local printers.");
        }
    }
}
