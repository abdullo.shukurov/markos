﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKeys
{
    public class PropertiesEntity
    {
        public string configName { get; set; }
        public string configKey { get; set; }
        public string gatewayAddress { get; set; }
        public string dbAddress { get; set; }
        public string dbName { get; set; }
        public string dbLogin { get; set; }
        public string dbPassword { get; set; }
        public string token { get; set; }
        public string omsId { get; set; }
    }
}
