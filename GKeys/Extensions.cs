﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKeys
{
    public static class Extensions
    {
        public static bool IsNullOrEmpty(this string value)
        {
            return value == null || value.Trim() == "";
        }
    }
}
