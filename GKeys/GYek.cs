﻿using CammonClasses.CR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKeys
{
    public class GYek
    {
        private const string DIRECTORY_PATH = @"c:\MARKOs";
        private const string FILE_PATH = "/MARKOs.json";
        private const string YEK = "MARKOs_0566";
        public const string LOCALHOST = "127.0.0.1";
        private const string VERSION_CODE = "v0.0.2";
        private const int VERSION_NUMBER = 0;
        public string psr = "doi234rjkdkj";
        public const string APP_NAME = "MARKOs";

        public GYek()
        {
            UpdateValues();
        }

        public string versionCode = VERSION_CODE;
        public int versionNumber = VERSION_NUMBER;
        public string appName = APP_NAME;
        public string key { set; get; }
        public string dbAddress { get; set; }
        public string dbName { get; set; }
        public string gatewayAddress { get; set; }
        public string productExtension { get; set; }
        public string printerType { get; set; }
        public string token { get; set; } = "";
        public string comPort { set; get; } = "COM3";
        public string omsId { get; set; } = "";
        public int XPrintCoordinate { get; set; } = 30;
        public int YPrintCoordinate { get; set; } = 30;
        public string SubscriptionStartDate { get; set; }
        public string SubscriptionEndDate { get; set; }



        private void UpdateValues()
        {
            var anonymJsonType = new
            {
                key,
                gatewayAddress,
                token,
                dbAddress,
                SubscriptionEndDate,
                SubscriptionStartDate,
                printerType,
                comPort,
                productExtension,
                omsId,
                dbName,
                XPrintCoordinate = "",
                YPrintCoordinate = ""
            };

            try
            {
                var jsonData = JsonConvert.DeserializeAnonymousType(Decrypt(ReadFromStorage()), anonymJsonType);
                if (jsonData == null)
                    return;
                dbName = Decrypt(jsonData.dbName);
                key = Decrypt(jsonData.key);
                comPort = Decrypt(jsonData.comPort);
                omsId = Decrypt(jsonData.omsId);
                gatewayAddress = Decrypt(jsonData.gatewayAddress);
                printerType = Decrypt(jsonData.printerType);
                productExtension = Decrypt(jsonData.productExtension);
                SubscriptionStartDate = Decrypt(jsonData.SubscriptionStartDate);
                SubscriptionEndDate = Decrypt(jsonData.SubscriptionEndDate);
                token = Decrypt(jsonData.token);
                dbAddress = Decrypt(jsonData.dbAddress);
                if (!string.IsNullOrEmpty(jsonData.XPrintCoordinate))
                    XPrintCoordinate = int.Parse(Decrypt(jsonData.XPrintCoordinate));
                if (!string.IsNullOrEmpty(jsonData.YPrintCoordinate))
                    YPrintCoordinate = int.Parse(Decrypt(jsonData.YPrintCoordinate));

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Json read exception {ex}");
            }
        }

        public bool UpdateDetails(PropertiesEntity entity)
        {
            key = entity.configKey;
            dbAddress = entity.dbAddress;
            dbName = entity.dbName;
            token = entity.token;
            omsId = entity.omsId;
            return SaveChanges();
        }

        public bool SaveChanges()
        {
            var anonymJsonType = new
            {
                dbAddress = Encrypt(dbAddress),
                dbName = Encrypt(dbName),
                token = Encrypt(token),
                omsId = Encrypt(omsId),
                SubscriptionStartDate = Encrypt(SubscriptionStartDate),
                SubscriptionEndDate = Encrypt(SubscriptionEndDate),
                productExtension = Encrypt(productExtension),
                printerType = Encrypt(printerType),
                XPrintCoordinate = Encrypt(XPrintCoordinate.ToString()),
                YPrintCoordinate = Encrypt(YPrintCoordinate.ToString()),
                comPort = Encrypt(comPort)
            };

            try
            {
                string json = JsonConvert.SerializeObject(anonymJsonType);
                return SaveToStorage(Encrypt(json));
            }
            catch
            {
                return false;
            }
        }

        private bool SaveToStorage(string json)
        {
            var createDirectory = Directory.CreateDirectory(DIRECTORY_PATH);
            var jsonPath = DIRECTORY_PATH + FILE_PATH;
            try
            {
                if (File.Exists(jsonPath))
                {
                    File.Delete(jsonPath);
                }

                using (FileStream fs = File.Create(jsonPath))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes(json);
                    fs.Write(info, 0, info.Length);
                }
                return true;
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        private string ReadFromStorage()
        {
            try
            {
                using (StreamReader sr = File.OpenText(DIRECTORY_PATH + FILE_PATH))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        return s;
                    }
                    return s;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
                return "";
            }
        }

        private string Encrypt(string data)
        {
            try
            {
                return En_De.En(data, YEK);
            }
            catch
            {
                return "";
            }
        }

        private string Decrypt(string data)
        {
            try
            {
                return En_De.De(data, YEK);
            }
            catch
            {
                return "";
            }
        }
    }
}
